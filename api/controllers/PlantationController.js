/**
 * PlantationController
 *
 * @description :: Server-side logic for managing plantations
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

	'plantation' : function (req, res) { 
		// res.locals.flash = _.clone(req.session.flash);
		res.view();
		// req.session.flash ={};
	},

	create: function (req, res, next) {

		Plantation.create({type:req.param('type'), latitude:req.param('latitude'), longitude:req.param('longitude'), status:req.param('status'), workTime:req.param('workTime'), description:req.param('description'), assignedTo:req.param('assignedTo'), hourSpent:req.param('hourSpent')}, function newPlant(err, aPlant) {

			if (err) {
				console.log("Plantation>create>" + err);
				return next(err);
			}

			res.ok({aPlant:aPlant});
			// res.ok();


		})
	},

	getall: function (req, res, next) {

		Plantation.find(function foundAllPlants(err, plantList) {
			if (err) {
				console.log("Plantation>getall>" + err);
				return next(err);
			}

			res.ok({plantList:plantList});
		})
	},

	findplantbyid: function (req, res, next) {

		Plantation.findOne({id:req.param('id')}, function foundPlant(err, aPlant) {
			if (err) {
				console.log("Plantation>findplantbyid>" + err);
				return next(err);
			}

			res.ok({aPlant: aPlant});
		})
	},

	findplantbyuserid: function(req, res, next) {

		Plantation.find({assignedTo:req.param('userId')}, function foundPlantAssignedToUser(err, plantList) {
			if (err) {
				console.log("Plantation>findplantbyuserid>" + err);
				return next(err);
			}

			res.ok({plantList:plantList});
		})
	},

	updateplant: function(req, res, next) {

		Plantation.update({id:req.param('id')}, {type:req.param('type'), latitude:req.param('latitude'), longitude:req.param('longitude'), status:req.param('status'), workTime:req.param('workTime'), description:req.param('description'), assignedTo:req.param('assignedTo'), hourSpent:req.param('hourSpent')}).exec(function updatedPlant(err, updatedPlant) {

			if(err) {
				console.log("Plantation>updateplant>" + err);
				return res.serverError(err);
			}

			if(!updatedPlant) {
				console.log("Plantation>updateplant>result>" + err);
				return res.notFound();
			}

			res.ok({updatedPlant:updatedPlant});
		})
	},

	deleteplant: function(req, res, next) {

		Plantation.findOne(req.param('id'), function foundPlant(err, aPlant) {
			if(err) {
				console.log("Plantation>deleteplant>foundUser>" + err);
				return next(err);
			}

			if(!aPlant) {
				console.log("Plantation>deleteplant>aPlant>" + err);
				return next('ID doesn\'t exist.');
			}

			Plantation.destroy(req.param('id'), function plantDeleted(err) {
				if(err) {
					console.log("Plantation>deleteplant>destroy>" + err);
					return next(err);
				}

				res.ok({aPlant:aPlant});

			})

		})
	},

	deleteallplants: function(req, res, next) {

		Plantation.find(function foundAllPlants(err, allPlants) {
			if(err) {
				console.log("Plantation>deleteall>foundAllPlants>" + err);
				return next(err);
			}

			if(!allPlants) {
				console.log("Plantation>deleteall>allPlants>" + err);
				return next('No plants found.');
			}

			for (i = 0; i < allPlants.length; i++) {

				Plantation.destroy(allPlants[i].id, function plantsDeleted(err) {
					if(err) {
						console.log("Plantation>deleteall>destroy>" + err);
						return next(err);
					}

				})
			}
			
			res.ok();
		})
	}




















	
};

