/**
 * TreesMapUserController
 *
 * @description :: Server-side logic for managing treesmapusers
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	'treesmapusers' : function (req, res) { 
		// res.locals.flash = _.clone(req.session.flash);
		res.view();
		// req.session.flash ={};
	},

	create: function (req, res, next) {

		TreesMapUser.create({name:req.param('name'), username:req.param('username'), password:req.param('password'), admin:req.param('admin')}, function newUser(err, aUser) {

			if (err) {
				console.log("TreesMapUser>create>" + err);
				return next(err);
			}

			res.ok({aUser: aUser});


		})
	},

	finduserbyid: function(req, res, next) {

		TreesMapUser.findOne({id:req.param('id')}, function foundUser(err, aUser) {
			if (err) {
				console.log("TreesMapUser>finduserbyid>" + err);
				return next(err);
			}

			res.ok({aUser: aUser});
		})
	},

	finduserbyname: function(req, res, next) {

		TreesMapUser.find({name:req.param('name')}, function foundUser(err, aUser) {
			if (err) {
				console.log("TreesMapUser>finduserbyname>" + err);
				return next(err);
			}

			res.ok({aUser: aUser});
		})
	},

	finduserbyusername: function(req, res, next) {

		TreesMapUser.findOne({username:req.param('username')}, function foundUser(err, aUser) {
			if (err) {
				console.log("TreesMapUser>finduserbyusername>" + err);
				return next(err);
			}

			res.ok({aUser: aUser});
		})
	},

	findalluser: function(req, res, next) {

		TreesMapUser.find(function foundAllUser(err, allUsers) {
			if (err) {
				console.log("TreesMapUser>findalluser>" + err);
				return next(err);
			}

			res.ok({allUsers: allUsers});
		})
	},

	updateuser: function(req, res, next) {

		TreesMapUser.update({name:req.param('name'), username:req.param('username'), password:req.param('password'), admin:req.param('admin')}, function(err, result) {

			if(err) {
				console.log("TreesMapUser>updateuser>" + err);
				return res.serverError(err);
			}

			if(!result) {
				console.log("TreesMapUser>updateuser>result>" + err);
				return res.notFound();
			}

			res.ok({result: result});
		})
	},

	deleteuser: function(req, res, next) {

		TreesMapUser.findOne(req.param('id'), function foundUser(err, aUser) {
			if(err) {
				console.log("TreesMapUser>deleteuser>foundUser>" + err);
				return next(err);
			}

			if(!aUser) {
				console.log("TreesMapUser>deleteuser>aUser>" + err);
				return next('ID doesn\'t exist.');
			}

			TreesMapUser.destroy(req.param('id'), function userDeleted(err) {
				if(err) {
					console.log("TreesMapUser>deleteuser>destroy>" + err);
					return next(err);
				}

				res.ok();

			})

		})
	},

	deleteallusers: function(req, res, next) {

		TreesMapUser.find(function foundAllUsers(err, allUsers) {
			if(err) {
				console.log("TreesMapUser>deleteall>foundAllUsers>" + err);
				return next(err);
			}

			if(!allUsers) {
				console.log("TreesMapUser>deleteall>allUsers>" + err);
				return next('No plants found.');
			}

			for (i = 0; i < allUsers.length; i++) {

				TreesMapUser.destroy(allUsers[i].id, function usersDeleted(err) {
					if(err) {
						console.log("TreesMapUser>deleteall>destroy>" + err);
						return next(err);
					}

				})
			}
			
			res.ok();
		})
	}



};

