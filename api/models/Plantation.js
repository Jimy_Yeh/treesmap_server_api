/**
 * Plantation.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

	autoPK: false,
	connection: 'plantationMysqlServer',
	tableName: 'plantation',
	schema: true,
	autoCreatedAt: false,
	autoUpdatedAt: false,

  	attributes: {
	  	id : {
			type: 'integer',
			primaryKey: true,
			unique:true,
			autoIncrement: true
	    },

	    type : {
	    	type: 'string'
	    },

	    latitude : {
	    	type: 'float'
	    },

	    longitude : {
	    	type: 'float'
	    },

	    status : {
	    	type: 'integer'
	    },

	    workTime : {
	    	type: 'integer'
	    },

	    description : {
	    	type: 'string'
	    },

	    assignedTo : {
	    	type: 'string'
	    },

	    hourSpent : {
	    	type: 'integer'
	    }



  	}
};

