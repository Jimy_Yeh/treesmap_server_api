/**
 * TreesMapUser.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

	autoPK: false,
	connection: 'treesmapuserMysqlServer',
	tableName: 'treesmapuser',
	schema: true,
	autoCreatedAt: false,
	autoUpdatedAt: false,

	attributes: {

		id : {
			type: 'integer',
			primaryKey: true,
			unique:true,
			autoIncrement: true
	    },

	    name : {
	    	type: 'string'
	    },

	    username : {
	    	type: 'string'
	    },

	    password : {
	    	type: 'string'
	    },

	    admin : {
	    	type: 'integer'
	    },

	}
};

